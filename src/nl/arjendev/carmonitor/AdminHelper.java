package nl.arjendev.carmonitor;


import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;

public class AdminHelper {
	
	private DevicePolicyManager mDPM;
	private ComponentName comName;
	
	/**
	 * Constructor, created DevicePolicyManager and ComponentName objects
	 * @param context
	 */
	public AdminHelper(Context context) {
		
		// Initialize variables
		mDPM = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
		comName = new ComponentName(context, DAdminReceiver.class);
		
	}
	
	/**
	 * Get the component name
	 */
	public ComponentName getComponentName() {
		return this.comName;
	}
	
	/**
	 * Get the DevicePolicyManager object
	 */
	public DevicePolicyManager getPolicyObject() {
		return this.mDPM;
	}
	
	/**
	 * Check whether the user had enabled admin permissions or not
	 */
	public boolean isDeviceAdmin() {
		
		// Check via DevicePolicyManager
		return this.mDPM.isAdminActive(this.comName);
		
	}
	
}
