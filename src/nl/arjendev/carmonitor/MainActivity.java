package nl.arjendev.carmonitor;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Check if device admin permission is set
        final AdminHelper aHelper = new AdminHelper(this);
        
        if (aHelper.isDeviceAdmin() == true) {
        	
        	// Clear button holder
        	((RelativeLayout) findViewById(R.id.buttonHolder)).removeAllViews();
        	
        	// Show a button to set them
        	Button pButton = new Button(this);
        	pButton.setText(R.string.revoke_permissions);
        	pButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// Launch the activity to have the user enable our admin.
                    aHelper.getPolicyObject().removeActiveAdmin(aHelper.getComponentName());
                    MainActivity.this.finish();
                    startActivity(MainActivity.this.getIntent());
				}
				
			});
        	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.width = LayoutParams.MATCH_PARENT;
			params.height = LayoutParams.WRAP_CONTENT;
        	pButton.setLayoutParams(params);
        	((RelativeLayout) findViewById(R.id.buttonHolder)).addView(pButton);
        	
        }
        else {
        	
        	// Clear button holder
        	((RelativeLayout) findViewById(R.id.buttonHolder)).removeAllViews();
        	
        	// Show a button to set them
        	Button pButton = new Button(this);
        	pButton.setText(R.string.set_permissions);
        	pButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// Launch the activity to have the user enable our admin.
                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, aHelper.getComponentName());
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, getString(R.string.device_policy_explanation));
                    startActivity(intent);
				}
			});
        	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.width = LayoutParams.MATCH_PARENT;
			params.height = LayoutParams.WRAP_CONTENT;
        	pButton.setLayoutParams(params);
        	((RelativeLayout) findViewById(R.id.buttonHolder)).addView(pButton);
        	
        }
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
