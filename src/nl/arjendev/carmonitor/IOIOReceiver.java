package nl.arjendev.carmonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

public class IOIOReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
				
		// Check if the device is being connected or disconnected
		if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
			
			// Get device info
			UsbDevice device  = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
			if (device.getVendorId() == 6991) {
				
				// Start service (with notification)
				context.startService(new Intent(context, CarService.class));
				
				// Turn on device
				PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	    		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Waking");
	    		wl.acquire();
	    		wl.release();
			
			}
			
		}
		else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
			
			// Get device info
			UsbDevice device  = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
			if (device.getVendorId() == 6991) {
				
				// Kill service
				context.stopService(new Intent(context, CarService.class));
				
				// Turn off screen
				final AdminHelper aHelper = new AdminHelper(context);
		        
		        if (aHelper.isDeviceAdmin() == true) {
		        	aHelper.getPolicyObject().lockNow();
		        }
		        
			}
			
		}
		
	}

}
