package nl.arjendev.carmonitor;

import ioio.lib.api.AnalogInput;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.IOIO;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.execution.Command;

public class CarService extends IOIOService {

	@SuppressWarnings("deprecation")
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		Log.d("CM", "Service started");
		super.onStartCommand(intent, flags, startId);
		
		Log.d("CM", "Services started");
		
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		if (intent != null && intent.getAction() != null && intent.getAction().equals("stop")) {
			// User clicked the notification. Need to stop the service.
			nm.cancel(0);
			stopSelf();
		} else {
			// Service starting. Create a notification.
			Notification notification = new Notification(R.drawable.ic_launcher, "CarMonitor service running", System.currentTimeMillis());
			notification.setLatestEventInfo(this, "CarMonitor Service", "Click to stop", PendingIntent.getService(this, 0, new Intent("stop", null, this, this.getClass()), 0));
			notification.flags |= Notification.FLAG_ONGOING_EVENT;
			nm.notify(0, notification);
		}
		
		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		nm.cancel(0);
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected IOIOLooper createIOIOLooper() {
		return new BaseIOIOLooper() {

			private AnalogInput rearPower, ignitionPower;
			private DigitalOutput ledPin;
			private boolean appStarted = false;

			@Override
			protected void setup() throws ConnectionLostException, InterruptedException {
				
				rearPower = ioio_.openAnalogInput(42);
				rearPower.setBuffer(1000);
				
				ignitionPower = ioio_.openAnalogInput(44);
				ignitionPower.setBuffer(1000);
				
				ledPin = ioio_.openDigitalOutput(IOIO.LED_PIN);
				
			}

			@Override
			public void loop() throws ConnectionLostException, InterruptedException {
				
				// Sleep for a second, this way it can fully buffer 1000 voltages because the sampling is 1khz
				Thread.sleep(1000);
				
				// Get available readings (should be ~1000)
				int available = rearPower.available();
				float avgVoltage = 0f;
				for (int i = 0; i < available; i++) avgVoltage += rearPower.getVoltageBuffered();
				avgVoltage = (avgVoltage / available);
				
				if (avgVoltage > 2.5f) {
					
					// Turn on led (false = on)
					ledPin.write(false);
					
					// Start nl.arjendev.rearcam
					if (appStarted == false) {
						Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("nl.arjendev.rearcam");
						startActivity(LaunchIntent);
					
						appStarted = true;
					}
					
				}
				else {
					
					// Check if rearcam was started, if it is stop it
					ledPin.write(true);
					
					if (appStarted == true) {
						
						// Stop app
						if (RootTools.isRootAvailable() && RootTools.isAccessGiven()) {
							Command command = new Command(0, "am force-stop nl.arjendev.rearcam")
				        	{

								@Override
								public void commandCompleted(int arg0, int arg1) {
									appStarted = false;
								}

								@Override
								public void commandOutput(int id, String line) {
								}

								@Override
								public void commandTerminated(int arg0, String arg1) {
									Toast.makeText(CarService.this, "Could not stop rearcam", Toast.LENGTH_LONG).show();
								}
				        	};
				        	try {
								RootTools.getShell(true).add(command);
							} catch (Exception e) {
								
							}
						}
						
					}
					
				}
				
				// Get readings for ignition power
				int ignAvailable = ignitionPower.available();
				float ignAvgVoltage = 0f;
				for (int i = 0; i < ignAvailable; i++) ignAvgVoltage += ignitionPower.getVoltageBuffered();
				ignAvgVoltage = (ignAvgVoltage / ignAvailable);
				
				if (ignAvgVoltage > 2.5f) {
					
					// Create wakelock
					Context context = CarService.this.getBaseContext();
					PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
					if (!pm.isScreenOn()) {
						PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Waking");
						wl.acquire();
						wl.release();
					}
					
				}
				
			}
			
		};
		
	}

}
